package com.howtodoinjava.demo;

import com.howtodoinjava.demo.model.EmployeeEntity;
import com.howtodoinjava.demo.repository.EmployeeRepository;
import com.howtodoinjava.demo.service.EmployeeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
//@DataJpaTest
@SpringBootTest
//@EnableCaching
public class EmployeeRepositoryTest 
{
	@Autowired
	EmployeeRepository repository;

	@Autowired
	EmployeeService service;

	@Test
	public void testRepository1(){
		System.out.println(service.getAllEmployees());
		System.out.println(service.getAllEmployees());
		System.out.println(service.getAllEmployees());
	}

	@Test
	public void testRepository() 
	{
		EmployeeEntity emp = new EmployeeEntity();
		emp.setFirstName("Lokesh");
		emp.setLastName("Gupta");
		emp.setEmail("howtodoinjava@gmail.com");
		
		repository.save(emp);
		
		System.out.println(emp);
		
		Assert.assertNotNull(emp.getId());
	}
}
